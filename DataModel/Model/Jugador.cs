﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataModel.Model.Enum;
using LiteNetLib;
using NetworkPacket.Enum;
using NetworkPacket.Packet;
using NetworkPacket.Packet.Login;
using NetworkPacket.Packet.Player;
using Server.Util;

namespace DataModel.Model
{
    /// <summary>
    ///     El modelo Jugador representa un jugador dentro dentro de PacMan
    /// </summary>
    [Table("Jugador")]
    public sealed class Jugador : BaseEntity
    {
        [NotMapped]
        public readonly NetPeer NetPeer;

        public Jugador()
        {
            
        }

        public Jugador(NetPeer peer)
        {
            NetPeer = peer;
        }

        public Jugador(LoginPacket loginPacket)
        {
            NombreUsuario = loginPacket.Username;
            Contraseña = loginPacket.Password;
        }

        public Jugador(RegisterPacket registerPacket)
        {
            CorreoElectrónico = registerPacket.Email;
            Contraseña = registerPacket.Password;
            NombreUsuario = registerPacket.Username;
        }

        /// <summary>
        ///     Id del jugador, siendo llave primaria de la tabla JUGADOR   
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdJugador { set; get; }

        /// <summary>
        ///     Correo electrónico con el que se registra el jugador 
        /// </summary>
        [Index(IsUnique = true)]
        [Required]
        [MinLength(1)]
        [MaxLength(150)]
        [StringLength (150)]
        [EmailAddress(ErrorMessage = "Ingresaste una dirección de correo inválida.")]
        public string CorreoElectrónico { get; set; }

        /// <summary>
        ///     Contraseña con el que se registra el jugador 
        /// </summary>
        [Required]
        [MinLength(1)]
        [MaxLength(100)]
        [StringLength(100)]
        public string Contraseña { get; set; }

        /// <summary>
        ///     NombreUsuario con el que se registra el jugador 
        /// </summary>
        [Required]
        [Index (IsUnique = true)]
        [MinLength(1)]
        [MaxLength(50)]
        [StringLength(50)]
        public string NombreUsuario { get; set; }

        [Column("EstadoJugador")]
        public string EstadoJugadorString
        {
            get => EstadoJugador.ToString();
            private set => EstadoJugador = value.ParseEnum<EstadoJugador>();
        }

        [NotMapped]
        public EstadoJugador EstadoJugador { get; set; } = EstadoJugador.EnVerificación;

        [NotMapped] public ActualScreen ActualScreen { get; set; } = ActualScreen.None;

        public ICollection<PuntajeTotal> PuntajesTotales { get; set; }

        public PlayerInfoPacket AsPlayerInfoPacket()
        {
            return new PlayerInfoPacket
            {
                IdPlayer = IdJugador,
                Username = NombreUsuario,
                Email = CorreoElectrónico,
                CreatedAt = CreatedAt?.ToString("O")
            };
        }
    }
}
