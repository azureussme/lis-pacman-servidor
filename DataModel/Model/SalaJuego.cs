﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataModel.Model.Enum;
using NetworkPacket.Enum;
using NetworkPacket.Packet;
using NetworkPacket.Packet.GameRoom;
using NetworkPacket.Packet.GameRoomList;

namespace DataModel.Model
{
    /// <summary>
    ///     El Modelo Sala representa el sitio donde se reunen los jugadores para llevar acabo la partida
    /// </summary>
    public class SalaJuego
    {
        private const int MÁXIMO_JUGADORES = 5;
        private readonly List<Jugador> _espectadores = new List<Jugador>();

        private readonly List<Jugador> _jugadores = new List<Jugador>();
        private DateTime _tiempoCreación = DateTime.Now;
        public string Contraseña;
        public EstadoSala EstadoSala = EstadoSala.Abierta;
        public Guid IdSalaJuego = Guid.NewGuid();
        public string Nombre;
        public bool PermitirEspectadores;
        public string Propietario;
        public event EventHandler<GameRoomUpdate> OnGameRoomStatusUpdated;

        public void AddPlayer(Jugador jugador)
        {
            if (_jugadores.Count >= MÁXIMO_JUGADORES) return;
            _jugadores.Add(jugador);

            jugador.ActualScreen = ActualScreen.GameRoom;

            NotifyChanges(new GameRoomUpdate
            {
                PlayerCount = GetPlayerCount(),
                Propertary = Propietario,
                GameRoomStatus = GameRoomStatus.PreGame
            });
        }

        public void RemovePlayer(Jugador jugador)
        {
            _jugadores.Remove(jugador);
            jugador.ActualScreen = ActualScreen.GameRoomList;
            
            if (GetPlayerCount() == 0)
            {
                NotifyChanges(new GameRoomUpdate
                {
                    GameRoomStatus = GameRoomStatus.Dissolved
                });
                
                // Notify spectators
            }
            else
            {
                // Era propietario? escoger uno nuevo.
                if (jugador.NombreUsuario == Propietario)
                {
                    Propietario = _jugadores[0].NombreUsuario;
                }
                
                NotifyChanges(new GameRoomUpdate
                {
                    PlayerCount = GetPlayerCount(),
                    Propertary = Propietario,
                    GameRoomStatus = GameRoomStatus.PreGame
                });
            }
        }

        public IEnumerable<Jugador> GetPlayers()
        {
            return _jugadores;
        }

        public int GetPlayerCount()
        {
            return _jugadores.Count;
        }

        public int GetSpectatorsCount()
        {
            return _espectadores.Count;
        }

        public GameRoomInfoPacket AsGameRoomInfoPacket()
        {
            return new GameRoomInfoPacket
            {
                HasPassword = !string.IsNullOrEmpty(Contraseña),
                IdGameRoom = IdSalaJuego.ToString(),
                Name = Nombre,
                PlayerCount = GetPlayerCount(),
                Propertary = Propietario,
                SpectatorsAllowed = PermitirEspectadores,
                SpectatorsCount = GetSpectatorsCount(),
                CreatedAt = _tiempoCreación.ToString("O")
            };
        }

        public bool HasPlayer(Jugador jugador)
        {
            return _jugadores.Contains(jugador);
        }

        public bool HasPlayer(int idJugador)
        {
            var jugador = _jugadores.FirstOrDefault(j => j.IdJugador == idJugador);
            return jugador != null;
        }

        public bool IsFull()
        {
            return _jugadores.Count >= MÁXIMO_JUGADORES;
        }

        private void NotifyChanges(GameRoomUpdate gameRoomUpdate)
        {
            gameRoomUpdate.IdGameRoom = IdSalaJuego.ToString();
            OnGameRoomStatusUpdated?.Invoke(this, gameRoomUpdate);
        }
    }
}