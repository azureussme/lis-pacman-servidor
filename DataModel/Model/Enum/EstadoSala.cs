namespace DataModel.Model.Enum
{
    public enum EstadoSala
    {
        Abierta,
        EnJuego
    }
}