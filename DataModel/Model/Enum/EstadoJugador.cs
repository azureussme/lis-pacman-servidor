namespace DataModel.Model.Enum
{
    public enum EstadoJugador
    {
        /// <summary>
        ///     Jugador que completó satisfactoriamente la verificación y puede hacer uso del sistema.
        /// </summary>
        Registrado,
        
        /// <summary>
        ///     Jugador que aún está en proceso de verificación y no puede hacer uso del sistema.
        /// </summary>
        EnVerificación,
        
        /// <summary>
        ///     Jugador que tiene prohibido el uso del sistema.
        /// </summary>
        Baneado,
    }
}