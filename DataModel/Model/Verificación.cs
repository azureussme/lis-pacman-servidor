﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataModel.Model
{
    /// <summary>
    ///     El Modelo Verificación representa el código que necesita el jugador para poder finalizar su registro 
    /// </summary>
    [Table ("Verificación")]
    public class Verificación
    {
        /// <summary>
        ///     id verificacion, siendo llave primaria de la tabla VERIFICACIÓN
        /// </summary>
        [Key]
        public int IdVerificación { get; set; }

        /// <summary>
        ///     Fecha en que se creo la verificación
        /// </summary>
        /// 
        public DateTime Fecha { get; set; } = DateTime.Now;

        /// <summary>
        ///     Código que se creó para que el usuario finalice su registro
        /// </summary>
        [MaxLength(5)]
        [StringLength(5)]
        public string Codigo { get; set; }

        public int IdJugador { get; set; }
        
        public Jugador Jugador { get; set; }
    }
}
