﻿using System.ComponentModel.DataAnnotations;
namespace DataModel.Model
{
    /// <summary>
    ///     El modelo Juego representa el la información de cada jugador dentro de la partida
    /// </summary>
    public class Juego
    {

        /// <summary>
        ///     Id del juego, siendo llave primaria de la tabla JUEGO 
        /// </summary>
        [Key]
        public int IdJuego { get; set; }

        /// <summary>
        ///     Puntaje del jugador dentro del juego 
        /// </summary> 
        public int Puntaje { get; set; }

        /// <summary>
        ///     Personaje que tiene el jugador dentro del juego
        /// </summary> 
        [MaxLength(50)] [StringLength(50)]
        public string Personaje { get; set; }

        /// <summary>
        ///    Posición que tiene el jugador dentro del juego 
        /// </summary> 
        public string posición { get; set; }


    }
}
