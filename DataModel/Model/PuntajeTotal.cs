﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NetworkPacket.Packet.Score;

namespace DataModel.Model
{
    /// <summary>
    ///     El Modelo PuntajeFinal representa el puntaje que a acomulado el jugador por cada partida 
    /// </summary>
    [Table("PuntajeTotal")]
    public class PuntajeTotal 
    {
        /// <summary>
        ///     id Puntaje total, siendo llave primaria de la tabla PUNTAJEFINAL
        /// </summary>
        [Key]
        public int IdPuntajeTotal { get; set; }

        /// <summary>
        ///     Puntaje que obtuvo el jugador al final de la partida 
        /// </summary>
        public int Puntaje { get; set; }

        /// <summary>
        ///     Cantidad de asesinatos que obtuvo el jugador al final de la partida 
        /// </summary>
        public int Asesinatos { get; set; }
        
        /// <summary>
        ///     Fecha en que se agrega la puntuación, la fecha que fue el juego del jugador  
        /// </summary>
        /// 
        public DateTime Fecha { get; set; } = DateTime.Today;
        
        public int IdJugador { get; set; }
        
        public Jugador Jugador { get; set; }
        
       

        public PuntajeTotal(ScorePacket scorePacket)
        {
            Puntaje = scorePacket.Score;
            Asesinatos = scorePacket.Kills;
            Fecha = Convert.ToDateTime(scorePacket.Date);

        }
    }
}
