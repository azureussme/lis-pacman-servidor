﻿using System;
using System.Data.Entity;
using MySql.Data.Entity;
using DataModel.Model;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Threading.Tasks;

namespace DataModel.Context
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class DataBaseContext : DbContext
    {
        /// <summary>
        ///     Crear query sobre el modelo Jugador 
        /// </summary>
        public DbSet<Jugador> Jugador { get; set; }

        /// <summary>
        ///     Crear query sobre el modelo V erificacion
        /// </summary>
        public DbSet<Verificación> Verificacion { get; set; }

        /// <summary>
        ///     Crear query sobre el modelo Puntaje final  
        /// </summary>
        public DbSet<PuntajeTotal> Puntajetotal { get; set; }

        public DataBaseContext() : base("name=connectionString")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<DataBaseContext>());
        }

        public static DataBaseContext Create()
        {
            return new DataBaseContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
        
        public override int SaveChanges()
        {
            AddTimestamps();
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync()
        {
            AddTimestamps();
            return await base.SaveChangesAsync();
        }

        private void AddTimestamps()
        {
            var entities = ChangeTracker.Entries()
                .Where(x => x.Entity is BaseEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (var entity in entities)
            {
                var now = DateTime.UtcNow; // current datetime

                if (entity.State == EntityState.Added)
                {
                    ((BaseEntity)entity.Entity).CreatedAt = now;
                }
                ((BaseEntity)entity.Entity).UpdatedAt = now;
            }
        }
    }
}
