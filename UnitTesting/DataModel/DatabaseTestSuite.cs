using System;
using System.Linq;
using DataModel.Context;
using DataModel.Model;
using NUnit.Framework;

namespace UnitTesting.DataModel
{
    [TestFixture]
    public class DatabaseTestSuite
    {
        [Test]
        public void TestCrearJugador()
        {
            using (var db = new DataBaseContext())
            {
                var jugador = new Jugador
                {
                    NombreUsuario = "ocharan",
                    Contraseña = "asdasd",
                    CorreoElectrónico = "ocharan@uv.mx"
                };
                db.Jugador.Add(jugador);
                db.SaveChanges();
                var jugador2 = db.Jugador.FirstOrDefault(j => j.NombreUsuario == jugador.NombreUsuario);
                Console.WriteLine(jugador2);
                Assert.IsNotNull(jugador2);
            }
        }
    }
}