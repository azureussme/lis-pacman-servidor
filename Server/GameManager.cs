using LiteNetLib;
using NetworkPacket.Packet.Game;
using Server.Interface;
using Server.Util;

namespace Server
{
    public class GameManager
    {
        private readonly INetworkPacket _networkPacket;
        private readonly IPlayerManager _playerManager;
        private readonly IGameRoomManager _gameRoomManager;
        
        public GameManager(INetworkPacket networkPacket, IPlayerManager playerManager, IGameRoomManager gameRoomManager)
        {
            _networkPacket = networkPacket;
            _playerManager = playerManager;
            _gameRoomManager = gameRoomManager;
            
            _networkPacket.RegisterListener<GameMovementPacket>(OnGameMovementPacketReceived);
            _networkPacket.RegisterListener<GameEventPacket>(OnGameEventPacketReceived);
        }

        private void OnGameMovementPacketReceived(GameMovementPacket gameMovementPacket, NetPeer netPeer)
        {
            var player = _playerManager.FindPlayer(netPeer);
            var gameCharacter = gameMovementPacket.GameCharacter;
            var x = gameMovementPacket.X;
            var y = gameMovementPacket.Y;
            var direction = gameMovementPacket.Direction;
            
            Logger.Log($"Movimiento de {player.NombreUsuario}:{gameCharacter} en ({x},{y}) con dirección {direction}");

            var sala = _gameRoomManager.GetGameRoomContainingPlayer(player);
            var jugadores = sala.GetPlayers();
            foreach (var jugador in jugadores)
            {
                if (jugador != player)
                {
                    _networkPacket.SendPacket(jugador.NetPeer, gameMovementPacket, DeliveryMethod.ReliableOrdered);
                }
            }
        }
        
        private void OnGameEventPacketReceived(GameEventPacket gameEventPacket, NetPeer netPeer)
        {
            var player = _playerManager.FindPlayer(netPeer);
            var sala = _gameRoomManager.GetGameRoomContainingPlayer(player);
            var jugadores = sala.GetPlayers();
            foreach (var jugador in jugadores)
            {
                if (jugador != player)
                {
                    _networkPacket.SendPacket(jugador.NetPeer, gameEventPacket, DeliveryMethod.ReliableOrdered);
                }
            }
        }
    }
}