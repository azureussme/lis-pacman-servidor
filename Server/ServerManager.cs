using System;
using System.Configuration;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using LiteNetLib;
using LiteNetLib.Utils;
using Server.Interface;
using Server.Util;

namespace Server
{
    /// <summary>
    ///     ServerLogic se encarga de la lógica principal del servidor, inicialización, escucha de peticiones y envío de
    ///     mensajes.
    /// </summary>
    public class ServerManager : INetEventListener, IPeerConnection, INetworkPacket
    {
        /// <summary>
        ///     Número máximo de conexiones que permite el servidor por defecto.
        /// </summary>
        private const int DefaultMaxConections = 100;

        /// <summary>
        ///     Puerto por defecto en el cual el servidor escuchará las peticiones.
        /// </summary>
        private const int DefaultPort = 9050;

        /// <summary>
        ///     Clave secreta de conexión por defecto.
        /// </summary>
        private const string DefaultConnectionKey = "LISPacman";

        /// <summary>
        ///     Clave secreta de conexión.
        /// </summary>
        private readonly string _connectionKey;

        /// <summary>
        ///     Número máximo de conexiones que permite el servidor.
        /// </summary>
        private readonly int _maxConnections;

        /// <summary>
        ///     Se encarga de escribir la información en los paquetes que se envían a través de la red.
        /// </summary>
        private readonly NetDataWriter _netDataWritter = new NetDataWriter();

        /// <summary>
        ///     Se encarga de procesar, serializar y deserializar los paquetes que se envían a través de la red.
        /// </summary>
        private readonly NetPacketProcessor _netPacketProcessor = new NetPacketProcessor();

        /// <summary>
        ///     Puerto en el cual el servidor escuchará las peticiones.
        /// </summary>
        // ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
        private readonly int _port;

        /// <summary>
        ///     Clase principal para todas las operaciones de red. Puede funcionar como cliente o servidor. Actuará
        ///     como servidor en este caso.
        /// </summary>
        private readonly NetManager _server;

        private IPeerConnection _peerConnectionListener;

        /// <summary>
        ///     Constructor de ServerLogic
        /// </summary>
        public ServerManager()
        {
            var maxConnections = ConfigurationManager.AppSettings["maxConnections"];
            _maxConnections = string.IsNullOrEmpty(maxConnections) ? DefaultMaxConections : int.Parse(maxConnections);
            var port = ConfigurationManager.AppSettings["port"];
            _port = string.IsNullOrEmpty(port) ? DefaultPort : int.Parse(port);
            _connectionKey = ConfigurationManager.AppSettings["connectionKey"] ?? DefaultConnectionKey;

            _server = new NetManager(this)
            {
                AutoRecycle = true
            };
            _server.Start(_port);

            Logger.Log("[S] Server started.");
        }

        public void OnPeerConnected(NetPeer peer)
        {
            if (peer == null)
            {
                return;
            }
            
            _peerConnectionListener.OnPeerConnected(peer);
        }

        public void OnPeerDisconnected(NetPeer netPeer, DisconnectInfo disconnectInfo)
        {
            _peerConnectionListener.OnPeerDisconnected(netPeer, disconnectInfo);
        }

        /// <summary>
        ///     Evento que ocurre cuando existe un error en la red.
        /// </summary>
        /// <param name="endPoint"></param>
        /// <param name="socketError">Información sobre el error del socket</param>
        public void OnNetworkError(IPEndPoint endPoint, SocketError socketError)
        {
            Logger.Log("[S] NetworkError: " + socketError);
        }

        /// <summary>
        ///     Evento que ocurre cuando se recibe un paquete en el servidor. Desempaqueta los paquetes y llama los
        ///     delegates asociados.
        /// </summary>
        /// <param name="peer">Cliente que envió el paquete</param>
        /// <param name="reader">Lector de paquete</param>
        /// <param name="deliveryMethod">Método de envío</param>
        public void OnNetworkReceive(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod)
        {
            try
            {
                _netPacketProcessor.ReadAllPackets(reader, peer);
            }
            catch (ParseException e)
            {
                Logger.Log($"[S] OnNetworkReceive error: {e.Message}");
            }
        }

        public void OnNetworkReceiveUnconnected(IPEndPoint remoteEndPoint, NetPacketReader reader,
            UnconnectedMessageType messageType)
        {
            Logger.Log("[S] OnNetworkReceiveUnconnected()");
        }

        /// <summary>
        ///     Evento que ocurre cuando se actualiza la latencia de un jugador.
        /// </summary>
        /// <param name="peer">Jugador que invocó el evento</param>
        /// <param name="latency">Latencia del jugador en millisegundos</param>
        public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
        {
        }

        /// <summary>
        ///     Evento que ocurre cuando un jugador desea conectarse al servidor. Si la cantidad de jugadores es menor
        ///     que MAX_CONECTIONS y contiene la CONNECTION_KEY correcta, permite la conexión. La deniega de otra forma.
        /// </summary>
        /// <param name="request">Solicitud de conexión</param>
        public void OnConnectionRequest(ConnectionRequest request)
        {
            if (_server.PeersCount < _maxConnections)
                request.AcceptIfKey(_connectionKey);
            else
                request.Reject();
        }

        public void SendPacket<T>(NetPeer peer, T packet, DeliveryMethod deliveryMethod) where T : class, new()
        {
            _netDataWritter.Reset();
            _netPacketProcessor.Write(_netDataWritter, packet);
            peer.Send(_netDataWritter, deliveryMethod);
        }

        public void RegisterListener<T>(Action<T, NetPeer> action) where T : class, new()
        {
            _netPacketProcessor.SubscribeReusable(action);
        }

        public void RegisterNestedType<T>() where T : class, INetSerializable, new()
        {
            _netPacketProcessor.RegisterNestedType(() => new T());
        }

        /// <summary>
        ///     El servidor escucha los eventos en la red.
        /// </summary>
        public void Listen()
        {
            _server.PollEvents();
            Thread.Sleep(15);
        }

        /// <summary>
        ///     Detiene la escucha del servidor.
        /// </summary>
        public void Stop()
        {
            _server.Stop();
        }

        public void SetPeerConnectionListener(IPeerConnection peerConnectionListener)
        {
            _peerConnectionListener = peerConnectionListener;
        }
    }
}