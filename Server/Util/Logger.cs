using System;

namespace Server.Util
{
    public static class Logger
    {
        public static void Log(string message)
        {
            Console.WriteLine("{0:MM/dd/yyyy hh:mm:ss.fff tt} - {1}", DateTime.Now, message);
        }
    }
}