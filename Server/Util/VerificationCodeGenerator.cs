using System;
using DataAccess.DAO;
using DataModel.Model;

namespace Server.Util
{
    public class VerificationCodeGenerator
    {
        private const int VerificationCodeMaxLength = 5;

        private readonly Jugador _jugador;
        private string _code;

        public VerificationCodeGenerator(Jugador jugador)
        {
            _jugador = jugador;
        }

        public void GenerateCodeForUser()
        {
            GenerateVerificationCode();
            SaveVerificationCodeInDatabase();
        }

        public void SendVerificationCodeToPlayer()
        {
            var email = new Email(_jugador.CorreoElectrónico,
                "Código de verificación de LIS Pacman para " + _jugador.NombreUsuario,
                "Tu código de verificación es: " + _code);
            email.SendEmail();
        }

        private void SaveVerificationCodeInDatabase()
        {
            var verificación = new Verificación
            {
                Codigo = _code,
                IdJugador = _jugador.IdJugador
            };

            Logger.Log("[S] Verification code " + _code + " created for " + _jugador.NombreUsuario);

            var verificaciónDAO = new VerificaciónDAO();
            verificaciónDAO.AgregarVerificación(verificación);
        }

        private void GenerateVerificationCode()
        {
            var random = new Random();
            var verificationCode = "";
            for (var i = 0; i < VerificationCodeMaxLength; i++) verificationCode += random.Next(0, 9);

            _code = verificationCode;
        }
    }
}