using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace Server.Util
{
    public class Email
    {
        private readonly string _body;
        private readonly bool _stmpClientEnableSsl;
        private readonly string _stmpClientHost;
        private readonly string _stmpClientPassword;
        private readonly int _stmpClientPort;
        private readonly bool _stmpClientUseDefaultCredentials;
        private readonly string _stmpClientUsername;
        private readonly string _subject;

        private readonly string _toEmail;

        public Email(string toEmail, string subject, string body)
        {
            _stmpClientUsername = ConfigurationManager.AppSettings["stmpClientUsername"];
            _stmpClientPassword = ConfigurationManager.AppSettings["stmpClientPassword"];
            _stmpClientHost = ConfigurationManager.AppSettings["stmpClientHost"];
            _stmpClientPort = int.Parse(ConfigurationManager.AppSettings["stmpClientPort"]);
            _stmpClientEnableSsl = bool.Parse(ConfigurationManager.AppSettings["stmpClientEnableSsl"]);
            _stmpClientUseDefaultCredentials =
                bool.Parse(ConfigurationManager.AppSettings["stmpClientUseDefaultCredentials"]);

            _toEmail = toEmail;
            _subject = subject;
            _body = body;
        }

        public void SendEmail()
        {
            var mailMessage = new MailMessage(
                _stmpClientUsername,
                _toEmail,
                _subject,
                _body);

            using (var smtpClient = new SmtpClient())
            {
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = _stmpClientUseDefaultCredentials;
                smtpClient.EnableSsl = _stmpClientEnableSsl;
                smtpClient.Host = _stmpClientHost;
                smtpClient.Port = _stmpClientPort;
                smtpClient.Credentials = new NetworkCredential(_stmpClientUsername, _stmpClientPassword);
                smtpClient.Send(mailMessage);
            }
        }
    }
}