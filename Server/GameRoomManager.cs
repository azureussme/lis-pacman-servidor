using System;
using System.Collections.Generic;
using System.Linq;
using DataModel.Model;
using LiteNetLib;
using NetworkPacket.Enum;
using NetworkPacket.Packet;
using NetworkPacket.Packet.GameRoom;
using NetworkPacket.Packet.GameRoomList;
using Server.Interface;
using Server.Util;

namespace Server
{
    public class GameRoomManager : IGameRoomManager
    {
        /// <summary>
        ///     Interface de red, para el registro de listeners y envío de paquetes.
        /// </summary>
        private readonly INetworkPacket _networkPacketInterface;
        
        /// <summary>
        ///     Interface del mánager de jugadores.
        /// </summary>
        private readonly IPlayerManager _playerManager;
        
        /// <summary>
        ///     Interface del mánager de salas de juego.
        /// </summary>
        private IGameChatManager _gameChatManager;

        /// <summary>
        ///     Lista de salas de juego.
        /// </summary>
        private readonly Dictionary<string, SalaJuego> _gameRooms = new Dictionary<string, SalaJuego>();

        public GameRoomManager(INetworkPacket networkPacketInterface, IPlayerManager playerManager)
        {
            _networkPacketInterface = networkPacketInterface;
            _networkPacketInterface.RegisterListener<CreateGameRoomPacket>(OnCreateGameRoomPacketReceived);
            _networkPacketInterface.RegisterListener<JoinGameRoomRequestPacket>(OnJoinGameRoomRequestPacketReceived);
            _networkPacketInterface.RegisterListener<GameRoomEventPacket>(OnGameRoomEventPacketReceived);
            _networkPacketInterface.RegisterListener<GameRoomPlayerEvent>(OnGameRoomPlayerEvent);

            _playerManager = playerManager;
        }

        private void OnCreateGameRoomPacketReceived(CreateGameRoomPacket createGameRoomPacket, NetPeer netPeer)
        {
            var requestingPlayer = _playerManager.FindPlayer(netPeer);
            
            var gameRoom = new SalaJuego
            {
                Nombre = createGameRoomPacket.Name,
                Contraseña = createGameRoomPacket.Password,
                Propietario = requestingPlayer.NombreUsuario,
                PermitirEspectadores = createGameRoomPacket.AllowSpectators
            };
            gameRoom.OnGameRoomStatusUpdated += OnGameRoomStatusUpdated;
            
            gameRoom.AddPlayer(requestingPlayer);

            _gameRooms.Add(gameRoom.IdSalaJuego.ToString(), gameRoom);

            Logger.Log($"Sala de juego con id {gameRoom.IdSalaJuego} creada por {requestingPlayer.NombreUsuario}.");
            Logger.Log($"Hay {_gameRooms.Count} salas de juego.");
            
            var gameRoomInfoPacket = gameRoom.AsGameRoomInfoPacket();
            
            Logger.Log("[S] Sending GameRoomInfoPacket to " + requestingPlayer.NombreUsuario);
            _networkPacketInterface.SendPacket(requestingPlayer.NetPeer, gameRoomInfoPacket, DeliveryMethod.ReliableOrdered);
            
            // Notificar la creación del juego a todos los jugadores que estén en la pantalla de lista de salas
            // de juego.
            var jugadores = _playerManager.FindPlayers(j => j.ActualScreen == ActualScreen.GameRoomList);
            foreach (var jugador in jugadores)
            {
                Logger.Log("[S] Sending GameRoomInfoPacket to " + jugador.NombreUsuario);
                _networkPacketInterface.SendPacket(jugador.NetPeer, gameRoomInfoPacket, DeliveryMethod.ReliableOrdered);
            }
            
            /*_gameChatManager.OnGameChatPlayerEventReceived(new GameChatPlayerEvent
            {
                IdGameRoom = gameRoom.IdSalaJuego.ToString(),
                IdPlayer = requestingPlayer.IdJugador,
                ChatEvent = ChatEvent.Join,
                Username = requestingPlayer.NombreUsuario
            }, netPeer);*/
        }

        private void OnJoinGameRoomRequestPacketReceived(JoinGameRoomRequestPacket joinGameRoomRequestPacket,
            NetPeer netPeer)
        {
            var sala = GetSala(joinGameRoomRequestPacket.IdGameRoom);
            if (sala == null) return;

            ReplyPacket<GameRoomListEvent> replyPacket;

            if (sala.IsFull())
            {
                replyPacket = new ReplyPacket<GameRoomListEvent>
                {
                    ReplyCode = GameRoomListEvent.GameRoomFull,
                    Message = "La sala está llena"
                };
                _networkPacketInterface.SendPacket(netPeer, replyPacket, DeliveryMethod.ReliableOrdered);
            }
            // TODO Más verificaciones.
            else
            {
                replyPacket = new ReplyPacket<GameRoomListEvent>
                {
                    ReplyCode = GameRoomListEvent.JoinSuccess
                };
                _networkPacketInterface.SendPacket(netPeer, replyPacket, DeliveryMethod.ReliableOrdered);

                var jugador = _playerManager.FindPlayer(netPeer);
                
                sala.AddPlayer(jugador);
                
                OnGameRoomPlayerEvent(new GameRoomPlayerEvent
                {
                    IdGameRoom = sala.IdSalaJuego.ToString(),
                    IdJugador = jugador.IdJugador,
                    Username = jugador.NombreUsuario,
                    PlayerEvent = PlayerEvent.Join
                }, netPeer);
            }
        }
        
        private void OnGameRoomEventPacketReceived(GameRoomEventPacket gameRoomEventPacket, NetPeer netPeer)
        {
            var idGameRoom = gameRoomEventPacket.IdGameRoom;
            var gameRoomEvent = gameRoomEventPacket.GameRoomEvent;
            var player = _playerManager.FindPlayer(netPeer);

            var sala = GetGameRoomContainingPlayer(player);

            if (gameRoomEvent == GameRoomEvent.LoadGame)
            {
                var loadGameEvent = new GameRoomEventPacket
                {
                    IdGameRoom = idGameRoom,
                    GameRoomEvent = GameRoomEvent.LoadGame
                };

                var jugadores = sala.GetPlayers();
                foreach (var jugador in jugadores)
                {
                    _networkPacketInterface.SendPacket(jugador.NetPeer, loadGameEvent, DeliveryMethod.ReliableOrdered);
                }
            }

            if (gameRoomEvent == GameRoomEvent.GameReady)
            {
                var startGameEvent = new GameRoomEventPacket
                {
                    IdGameRoom = idGameRoom,
                    GameRoomEvent = GameRoomEvent.StartGame
                };
                    
                var jugadores = sala.GetPlayers();
                foreach (var jugador in jugadores)
                {
                    _networkPacketInterface.SendPacket(jugador.NetPeer, startGameEvent, DeliveryMethod.ReliableOrdered);
                }
            }
        }
        
        private void OnGameRoomPlayerEvent(GameRoomPlayerEvent gameRoomPlayerEvent, NetPeer netPeer)
        {
            var idSala = gameRoomPlayerEvent.IdGameRoom;
            var idJugador = gameRoomPlayerEvent.IdJugador;
            var username = gameRoomPlayerEvent.Username;
            var playerEvent = gameRoomPlayerEvent.PlayerEvent;

            var jugador = _playerManager.FindPlayer(netPeer);
            
            Logger.Log($"Evento {playerEvent} de {username} en la sala {idSala}.");

            var sala = GetSala(idSala);

            var players = sala?.GetPlayers();
            if (players == null) return;

            // Solo si es de salida.
            if (playerEvent == PlayerEvent.Exit)
            {
                sala.RemovePlayer(jugador);
            
                Logger.Log($"El jugador {username} salió de la sala {idSala}.");

                // La sala se quedó sin jugadores.
                if (sala.GetPlayerCount() <= 0)
                {
                    _gameRooms.Remove(idSala);
                    Logger.Log($"La sala {idSala} se disolvió.");
                    return;
                }
            }

            foreach (var player in players)
            {
                if (playerEvent == PlayerEvent.Exit && player.IdJugador == idJugador)
                {
                    continue;
                }

                if (player.IdJugador != idJugador)
                {
                    Logger.Log($"Enviando evento de sala {playerEvent} de {username} a {player.NombreUsuario} en la sala {idSala}.");
                    _networkPacketInterface.SendPacket(player.NetPeer, gameRoomPlayerEvent, DeliveryMethod.ReliableOrdered);
                }

                var gRpe = new GameRoomPlayerEvent
                {
                    IdJugador = player.IdJugador,
                    Username = player.NombreUsuario,
                    IdGameRoom = idSala,
                    PlayerEvent = playerEvent
                };
                
                Logger.Log($"Enviando evento de sala {playerEvent} de {player.NombreUsuario} a {username} en la sala {idSala}.");
                _networkPacketInterface.SendPacket(netPeer, gRpe, DeliveryMethod.ReliableOrdered);
            }
        }

        private void OnPlayerJoin(GameRoomPlayerEvent gameRoomPlayerEvent)
        {
            var idSala = gameRoomPlayerEvent.IdGameRoom;
            var idJugador = gameRoomPlayerEvent.IdJugador;

            var player = _playerManager.FindPlayer(idJugador);
            if (player == null) return;

            var sala = GetSala(idSala);
            if (sala == null) return;
            
            Logger.Log($"El jugador {player.NombreUsuario} entró a la sala {sala.IdSalaJuego}.");

            var jugadores = sala.GetPlayers();
            
            foreach (var jugador in jugadores)
            {
                if (jugador.IdJugador != player.IdJugador)
                {
                    Logger.Log($"Enviando evento de sala {gameRoomPlayerEvent.PlayerEvent}:{player.NombreUsuario} a {jugador.NombreUsuario} en la sala {sala.IdSalaJuego}.");
                    _networkPacketInterface.SendPacket(jugador.NetPeer, gameRoomPlayerEvent, DeliveryMethod.ReliableOrdered);
                }
                
                var gRep = new GameRoomPlayerEvent
                {
                    IdGameRoom = gameRoomPlayerEvent.IdGameRoom,
                    IdJugador = jugador.IdJugador,
                    PlayerEvent = PlayerEvent.Join,
                    Username = jugador.NombreUsuario
                };
                
                Logger.Log($"Enviando evento de sala {gameRoomPlayerEvent.PlayerEvent}:{jugador.NombreUsuario} a {player.NombreUsuario} en la sala {sala.IdSalaJuego}.");
                _networkPacketInterface.SendPacket(player.NetPeer, gRep, DeliveryMethod.ReliableOrdered);
            }
        }

        private void OnPlayerExit(GameRoomPlayerEvent gameRoomPlayerEvent)
        {
            var idSala = gameRoomPlayerEvent.IdGameRoom;
            var idJugador = gameRoomPlayerEvent.IdJugador;
            
            var player = _playerManager.FindPlayer(idJugador);
            if (player == null) return;

            var sala = GetSala(idSala);
            if (sala == null) return;
            
            sala.RemovePlayer(player);
            
            Logger.Log($"El jugador {player.NombreUsuario} salió de la sala {sala.IdSalaJuego}.");

            // La sala se quedó sin jugadores.
            if (sala.GetPlayerCount() <= 0)
            {
                _gameRooms.Remove(idSala);
                Logger.Log($"La sala {sala.IdSalaJuego} se disolvió.");
            }
            
            // La sala aún tiene jugadores.
            else
            {
                Logger.Log($"Aún hay {sala.GetPlayerCount()} jugador(es) en la sala {sala.IdSalaJuego}");
                foreach (var p in sala.GetPlayers())
                {
                    Logger.Log($"Enviando evento de sala {gameRoomPlayerEvent.PlayerEvent} de {player.NombreUsuario} en la sala {sala.IdSalaJuego}.");
                    _networkPacketInterface.SendPacket(p.NetPeer, gameRoomPlayerEvent, DeliveryMethod.ReliableOrdered);
                }
            }
        }

        public void SendGameRoomListToPlayer(NetPeer netPeer)
        {
            foreach (var gameRoomInfoPacket in _gameRooms.Values.Select(gameRoom => gameRoom.AsGameRoomInfoPacket()))
            {
                _networkPacketInterface.SendPacket(netPeer, gameRoomInfoPacket, DeliveryMethod.ReliableOrdered);
            }
        }

        public void RemovePlayer(Jugador player)
        {
            var sala = GetGameRoomContainingPlayer(player.IdJugador);
            if (sala == null) return;
            
            OnGameRoomPlayerEvent(new GameRoomPlayerEvent
            {
                IdGameRoom = sala.IdSalaJuego.ToString(),
                IdJugador = player.IdJugador,
                Username = player.NombreUsuario,
                PlayerEvent = PlayerEvent.Exit
            }, player.NetPeer);
        }

        public SalaJuego GetSala(string idGameRoom)
        {
            return _gameRooms.Values.FirstOrDefault(s => s.IdSalaJuego.ToString() == idGameRoom);
        }

        public void SetGameChatManager(IGameChatManager gameChatManager)
        {
            _gameChatManager = gameChatManager;
        }

        public SalaJuego GetGameRoomContainingPlayer(Jugador player)
        {
            return _gameRooms.Values.FirstOrDefault(gameRoom => gameRoom.HasPlayer(player));
        }

        public SalaJuego GetGameRoomContainingPlayer(int idPlayer)
        {
            return _gameRooms.Values.FirstOrDefault(gameRoom => gameRoom.HasPlayer(idPlayer));
        }

        private void OnGameRoomStatusUpdated(object sender, GameRoomUpdate gameRoomUpdate)
        {
            var players = _playerManager.FindPlayers(p => p.ActualScreen == ActualScreen.GameRoomList);

            foreach (var p in players)
            {
                Logger.Log($"Enviando evento de sala a {p.NombreUsuario} sobre sala {gameRoomUpdate.IdGameRoom}.");
                _networkPacketInterface.SendPacket(p.NetPeer, gameRoomUpdate, DeliveryMethod.ReliableOrdered);
            }
        }
    }
}