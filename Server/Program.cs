﻿namespace Server
{
    internal static class Program
    {
        public static void Main()
        {
            var server = new Server();
            server.Listen();
            server.Stop();
        }
    }
}