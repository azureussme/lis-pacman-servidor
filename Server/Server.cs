using System;
using Server.Interface;
using Server.Util;
// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace Server
{
    public class Server
    {
        private readonly ServerManager _serverManager;
        private readonly LoginManager _loginManager;
        private readonly PlayerManager _playerManager;
        private readonly IGameRoomManager _gameRoomManager;
        private readonly IGameChatManager _gameChatManager;
        private readonly GameManager _gameManager;
        private readonly IScoreManager _scoreManager;

        public Server()
        {
            _serverManager = new ServerManager();
            _playerManager = new PlayerManager(_serverManager);
            _serverManager.SetPeerConnectionListener(_playerManager);
            
            _gameRoomManager = new GameRoomManager(_serverManager, _playerManager);
            _playerManager.SetGameRoomManagerListener(_gameRoomManager);
            
            _loginManager = new LoginManager(_serverManager, _playerManager);
            
            _gameChatManager = new GameChatManager(_serverManager, _playerManager);
            _playerManager.SetGameChatManager(_gameChatManager);
            _gameRoomManager.SetGameChatManager(_gameChatManager);
            
            _gameManager = new GameManager(_serverManager, _playerManager, _gameRoomManager);
            
            _scoreManager = new ScoreManager(_serverManager, _playerManager, _gameRoomManager);

            var playerCount = _playerManager.GetRegisteredPlayerCount();
            Logger.Log($"There is {playerCount} players registered.");
        }

        /// <summary>
        ///     Permite que el servidor comience a escuchar peticiones.
        /// </summary>
        public void Listen()
        {
            while (!Console.KeyAvailable)
            {
                _serverManager.Listen();
            }
        }

        /// <summary>
        ///     Detiene el servidor.
        /// </summary>
        public void Stop()
        {
            _serverManager.Stop();
        }
    }
}