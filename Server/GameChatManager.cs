using LiteNetLib;
using NetworkPacket.Packet.GameChat;
using Server.Interface;
using Server.Util;

namespace Server
{
    /// <summary>
    ///     Clase que maneja los eventos de recepción y envío de paquetes de mensajes del chat.
    /// </summary>
    public class GameChatManager : IGameChatManager
    {
        /// <summary>
        ///     Interface de red, para el registro de listeners y envío de paquetes.
        /// </summary>
        private readonly INetworkPacket _networkPacket;
        
        /// <summary>
        ///     Interface del mánager de jugadores.
        /// </summary>
        private readonly IPlayerManager _playerManager;

        public GameChatManager(INetworkPacket networkPacket, IPlayerManager playerManager)
        {
            _networkPacket = networkPacket;
            _playerManager = playerManager;
            
            _networkPacket.RegisterListener<GameChatMessagePacket>(OnGameChatMessagePacketReceived);
            _networkPacket.RegisterListener<GameChatPlayerEvent>(OnGameChatPlayerEventReceived);
        }

        public void OnGameChatPlayerEventReceived(GameChatPlayerEvent gameChatPlayerEvent, NetPeer netPeer)
        {
            var idPlayer = gameChatPlayerEvent.IdPlayer;
            var username = gameChatPlayerEvent.Username;
            var idGameRoom = gameChatPlayerEvent.IdGameRoom;
            var chatEvent = gameChatPlayerEvent.ChatEvent;

            Logger.Log(string.IsNullOrEmpty(idGameRoom)
                ? $"GameChatPlayerEvent {chatEvent} from {username}"
                : $"GameChatPlayerEvent {chatEvent} from {username} on {idGameRoom}");

            var jugadores = string.IsNullOrEmpty(idGameRoom) 
                ? _playerManager.FindPlayersInChat() 
                : _playerManager.FindPlayersInChat(idGameRoom);

            if (jugadores == null)
            {
                return;
            }

            foreach (var jugador in jugadores)
            {
                // No enviar mensajes de salida al jugador que acaba de salir.
                if (chatEvent == ChatEvent.Exit && jugador.IdJugador == idPlayer)
                {
                    continue;
                }

                // Notificar al jugador en chat el Join del nuevo.
                if (jugador.IdJugador != idPlayer)
                {
                    Logger.Log(string.IsNullOrEmpty(idGameRoom)
                        ? $"Enviando evento de chat {chatEvent} de {username} a {jugador.NombreUsuario}"
                        : $"Enviando evento de chat {chatEvent} de {username} a {jugador.NombreUsuario} en la sala {idGameRoom}");
                    _networkPacket.SendPacket(jugador.NetPeer, gameChatPlayerEvent, DeliveryMethod.ReliableOrdered);
                }
                
                // Enviar información de jugador al que acaba de conectarse.
                var gCpe = new GameChatPlayerEvent
                {
                    IdPlayer = jugador.IdJugador,
                    Username = jugador.NombreUsuario,
                    IdGameRoom = idGameRoom,
                    ChatEvent = chatEvent
                };
                
                Logger.Log(string.IsNullOrEmpty(idGameRoom)
                    ? $"Enviando evento de chat {chatEvent} de {jugador.NombreUsuario} al nuevo jugador {username}"
                    : $"Enviando evento de chat {chatEvent} de {jugador.NombreUsuario} al nuevo jugador {username} en la sala {idGameRoom}");
                _networkPacket.SendPacket(netPeer, gCpe, DeliveryMethod.ReliableOrdered); 
            }
        }

        private void OnGameChatMessagePacketReceived(GameChatMessagePacket gameChatMessagePacket, NetPeer netPeer)
        {
            var idPlayer = gameChatMessagePacket.IdPlayer;
            var idGameRoom = gameChatMessagePacket.IdGameRoom;
            var message = gameChatMessagePacket.Message;

            var player = _playerManager.FindPlayer(idPlayer);
            var username = player.NombreUsuario;
            
            var jugadores =
                _playerManager.FindPlayersInChat(idGameRoom);

            if (jugadores == null) return;

            Logger.Log(string.IsNullOrEmpty(idGameRoom)
                ? $"Mensaje de {username}: {message}"
                : $"Mensaje en la sala {idGameRoom} de {username}: {message}");

            foreach (var jugador in jugadores)
            {
                if (jugador == player)
                {
                    continue;
                }

                Logger.Log($"Enviando mensaje de {username} a {jugador.NombreUsuario}");
                _networkPacket.SendPacket(jugador.NetPeer, gameChatMessagePacket, DeliveryMethod.ReliableOrdered);
            }
        }
    }
}