using System;
using System.Collections.Generic;
using DataModel.Model;
using LiteNetLib;

namespace Server.Interface
{
    public interface IScoreManager
    {
        /// <summary>
        ///     Solcita los Score del Jugador 
        /// </summary>
        void SendScoreToPlayer(Jugador player);
        
        /// <summary>
        ///     Muestra la lista de puntuación de los jugadores
        /// </summary>
        void SendScoreToPlayer(NetPeer netPeer);
    }
}
