using System;
using System.Collections.Generic;
using DataModel.Model;
using LiteNetLib;

namespace Server.Interface
{
    /// <summary>
    ///     Interface que expone la funcionalidad de PlayerManager para recibir información
    ///     sobre los jugadores conectados al servidor.
    /// </summary>
    public interface IPlayerManager
    {
        /// <summary>
        ///     Devuelve un jugador por su id.
        /// </summary>
        /// <param name="idPlayer"></param>
        /// <returns></returns>
        Jugador FindPlayer(int idPlayer);
        
        /// <summary>
        ///     Devuelve un jugador asociado a un NetPeer
        /// </summary>
        /// <param name="netPeer">Cliente de red</param>
        /// <returns>Jugador asociado al Cliente de red</returns>
        Jugador FindPlayer(NetPeer netPeer);
        
        /// <summary>
        ///     Devuelve una lista de jugadores que cumplen con un predicado.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>Lista de jugadores que cumplen con el predicado</returns>
        IEnumerable<Jugador> FindPlayers(Func<Jugador, bool> predicate);
        
        /// <summary>
        ///     Envía la información de un jugador a un jugador.
        /// </summary>
        /// <param name="player">Jugador al que se le enviará la información</param>
        void SendPlayerInfoPacketToPlayer(Jugador player);
        
        /// <summary>
        ///     Devuelve una lista de jugadores que se encuentran en el chat de lista de juegos o
        ///     en el chat de una sala con el id especificado.
        /// </summary>
        /// <param name="idGameRoom">[opcional] id de la sala de juego</param>
        /// <returns>Lista de jugadores en un chat</returns>
        IEnumerable<Jugador> FindPlayersInChat(string idGameRoom = null);
    }
}