using DataModel.Model;
using LiteNetLib;

namespace Server.Interface
{
    /// <summary>
    ///     Interface que expone la funcionalidad de GameRoomManager para enviar listas de salas y
    ///     eliminar un jugador de una sala por desconexión.
    /// </summary>
    public interface IGameRoomManager
    {
        /// <summary>
        ///     Envía la lista de las salas de juegos a un jugador.
        /// </summary>
        /// <param name="netPeer">Cliente al que se le enviará la lista</param>
        void SendGameRoomListToPlayer(NetPeer netPeer);
        
        /// <summary>
        ///     Elimina a un jugador de una sala de juego, si es que está dentro de una.
        /// </summary>
        /// <param name="player">Jugador a eliminar</param>
        void RemovePlayer(Jugador player);
        
        /// <summary>
        ///     Proporciona la sala asociada al id proporcionado.
        /// </summary>
        /// <param name="idGameRoom">Id de la sala</param>
        /// <returns></returns>
        SalaJuego GetSala(string idGameRoom);
        
        /// <summary>
        ///     Establece la interface de GameChatManager
        /// </summary>
        /// <param name="gameChatManager">Interface de GameChatManager</param>
        void SetGameChatManager(IGameChatManager gameChatManager);

        /// <summary>
        ///     Indica si un jugador está dentro de una sala de juego.
        /// </summary>
        /// <param name="player">Jugador a buscar</param>
        /// <returns>Verdadero si el jugador está dentro de una sala</returns>
        SalaJuego GetGameRoomContainingPlayer(Jugador player);

        /// <summary>
        ///     Indica si un jugador está dentro de una sala de juego.
        /// </summary>
        /// <param name="idPlayer">Id del jugador</param>
        /// <returns>Verdadero si el jugador está dentro de una sala</returns>
        SalaJuego GetGameRoomContainingPlayer(int idPlayer);
    }
}