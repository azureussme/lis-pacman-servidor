using LiteNetLib;
using NetworkPacket.Packet.GameChat;

namespace Server.Interface
{
    /// <summary>
    ///     Interface que expone la funcionalidad de GameChatManager para enviar/recibir mensajes y
    ///     eventos de los jugadores conectados al chat del juego.
    /// </summary>
    public interface IGameChatManager
    {
        /// <summary>
        ///     Evento que ocurre cuando el servidor recibe un evento de jugador en un chat de juego.
        /// </summary>
        /// <param name="gameChatPlayerEvent">Evento de jugador en chat de juego</param>
        /// <param name="netPeer">Cliente que envió el evento</param>
        void OnGameChatPlayerEventReceived(GameChatPlayerEvent gameChatPlayerEvent, NetPeer netPeer);
    }
}