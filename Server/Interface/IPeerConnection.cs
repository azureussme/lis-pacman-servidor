using LiteNetLib;

namespace Server.Interface
{
    /// <summary>
    ///     Interface que expone la funcionalidad de ServerManager para manejar los eventos de conexión
    ///     y desconexión de los clientes.
    /// </summary>
    public interface IPeerConnection
    {
        /// <summary>
        ///     Evento que ocurre cuando un cliente se conecta al servidor.
        /// </summary>
        /// <param name="netPeer">Cliente que se conectó</param>
        void OnPeerConnected(NetPeer netPeer);

        /// <summary>
        ///     Evento que ocurre cuando un cliente se desconecta del servidor.
        /// </summary>
        /// <param name="netPeer">Cliente que se desconectó</param>
        /// <param name="disconnectInfo">Información acerca de la desconexión</param>
        void OnPeerDisconnected(NetPeer netPeer, DisconnectInfo disconnectInfo);
    }
}