using System;
using LiteNetLib;
using LiteNetLib.Utils;

namespace Server.Interface
{
    /// <summary>
    ///     Interface que expone la funcionalidad de ServerManager para el envío y recepción
    ///     de paquetes de red.
    /// </summary>
    public interface INetworkPacket
    {
        /// <summary>
        ///     Envía un paquete a un cliente.
        /// </summary>
        /// <param name="peer">Cliente al que se enviará el paquete</param>
        /// <param name="packet">Paquete que se enviará</param>
        /// <param name="deliveryMethod">Método de entrega del paquete</param>
        /// <typeparam name="T">Tipo del paquete</typeparam>
        void SendPacket<T>(NetPeer peer, T packet, DeliveryMethod deliveryMethod) where T : class, new();

        /// <summary>
        ///     Registra un callback para que se ejecute cuando se reciba determinado paquete de red.
        /// </summary>
        /// <param name="action">Callback a ejecutar</param>
        /// <typeparam name="T">Tipo de paquete de red</typeparam>
        void RegisterListener<T>(Action<T, NetPeer> action) where T : class, new();
        
        /// <summary>
        ///     Registra un tipo de paquete de red anidado.
        /// </summary>
        /// <typeparam name="T">Tipo de paquete de red anidado.</typeparam>
        void RegisterNestedType<T>() where T : class, INetSerializable, new();
    }
}