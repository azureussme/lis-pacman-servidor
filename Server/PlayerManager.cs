using System;
using System.Collections.Generic;
using System.Linq;
using DataAccess.DAO;
using DataModel.Model;
using JetBrains.Annotations;
using LiteNetLib;
using NetworkPacket.Enum;
using NetworkPacket.Packet.Game;
using NetworkPacket.Packet.GameChat;
using NetworkPacket.Packet.Player;
using Server.Interface;
using Server.Util;

namespace Server
{
    public class PlayerManager : IPlayerManager, IPeerConnection
    {
        /// <summary>
        ///     Lista de jugadores relacionados a su NetPeer.
        /// </summary>
        private readonly Dictionary<NetPeer, Jugador> _players = new Dictionary<NetPeer, Jugador>();
        
        /// <summary>
        ///     Interface de red, para el registro de listeners y envío de paquetes.
        /// </summary>
        private readonly INetworkPacket _networkPacketInterface;
        
        /// <summary>
        ///     Interface del mánager de salas de juego.
        /// </summary>
        private IGameRoomManager _gameRoomManager;
        
        /// <summary>
        ///     Interface del mánager del chat del juego.
        /// </summary>
        private IGameChatManager _gameChatManager;

        public PlayerManager(INetworkPacket networkPacketInterface)
        {
            _networkPacketInterface = networkPacketInterface;
            _networkPacketInterface.RegisterListener<PlayerStatus>(OnPlayerStatusPacketReceived);
        }

        public void OnPeerConnected(NetPeer netPeer)
        {
            if (_players.ContainsKey(netPeer))
            {
                return;
            }
            
            var jugador = new Jugador(netPeer);
            _players.Add(netPeer, jugador);
            Logger.Log("[S] Client connected: " + netPeer.EndPoint + " with ID: " + netPeer.Id);
            Logger.Log("[S] There is " + _players.Count + " players connected.");
        }

        public void OnPeerDisconnected(NetPeer netPeer, DisconnectInfo disconnectInfo)
        {
            if (!_players.ContainsKey(netPeer))
            {
                return;
            }

            var player = _players[netPeer];
            _gameRoomManager.RemovePlayer(player);
            _players.Remove(netPeer);
            Logger.Log("[S] Client disconnected: " + netPeer.EndPoint + " with ID: " + netPeer.Id);
            Logger.Log("[S] Client disconnected: " + disconnectInfo.Reason);
            Logger.Log("[S] There is " + _players.Count + " players connected.");
        }

        public Jugador FindPlayer(int idPlayer)
        {
            return _players.Values.SingleOrDefault(j => j.IdJugador == idPlayer);
        }

        [CanBeNull]
        public Jugador FindPlayer(NetPeer netPeer)
        {
            return _players.ContainsKey(netPeer) ? _players[netPeer] : null;
        }

        public IEnumerable<Jugador> FindPlayers(Func<Jugador, bool> predicate)
        {
            return _players.Values.Where(predicate);
        }

        public void SendPlayerInfoPacketToPlayer(Jugador player)
        {
            Logger.Log($"[S] Sending PlayerInfoPacket to <{player.NombreUsuario}> with id {player.IdJugador}");
            var playerInfoPacket = player.AsPlayerInfoPacket();
            _networkPacketInterface.SendPacket(player.NetPeer, playerInfoPacket, DeliveryMethod.ReliableOrdered);
        }

        public IEnumerable<Jugador> FindPlayersInChat(string idGameRoom = null)
        {
            return string.IsNullOrEmpty(idGameRoom) 
                // Todos los jugadores en la lista de salas de juego.
                ? FindPlayers(j => j.ActualScreen == ActualScreen.GameRoomList/* && j.IdJugador != idPlayerException*/)
                // Todos los jugadores en una sala de juego.
                : _gameRoomManager.GetSala(idGameRoom)?.GetPlayers();
        }

        private void OnPlayerStatusPacketReceived(PlayerStatus playerStatus, NetPeer netPeer)
        {
            Logger.Log($"[S] OnPlayerStatusPacketReceived from {playerStatus.IdJugador}");
            
            var player = FindPlayer(netPeer);

            if (player == null)
            {
                return;
            }

            // Recibí un paquete con un IdJugador == 0, el cliente cerró sesión
            if (playerStatus.IdJugador == 0)
            {
                if (!string.IsNullOrEmpty(player.NombreUsuario))
                {
                    Logger.Log($"[S] Player {player.NombreUsuario} logged out.");
                }
                player.IdJugador = 0;
            }
            else
            {
                // Recibí un paquete con un IdJugador, pero el que tengo guardado asociado a su NetPeer no tiene la
                // información del usuario. Consigo su información en base de datos. Esto ocurre en una reconexión.
                if (player.IdJugador != playerStatus.IdJugador)
                {
                    var jugadorDAO = new JugadorDAO();
                    var jugador = jugadorDAO.GetJugadorPorId(playerStatus.IdJugador);
                    player.IdJugador = playerStatus.IdJugador;
                    player.CorreoElectrónico = jugador.CorreoElectrónico;
                    player.EstadoJugador = jugador.EstadoJugador;
                    player.NombreUsuario = jugador.NombreUsuario;
                    player.ActualScreen = playerStatus.ActualScreen;
                    Logger.Log(
                        $"[S] playerId {playerStatus.IdJugador} is {player.NombreUsuario} on {playerStatus.ActualScreen}.");
                }
                // Recibo una actualización de actualScreen, 
                else
                {
                    player.ActualScreen = playerStatus.ActualScreen;
                    switch (player.ActualScreen)
                    {
                        case ActualScreen.None:
                            break;
                        case ActualScreen.GameRoomList:
                            _gameRoomManager.SendGameRoomListToPlayer(netPeer);
                            break;
                        case ActualScreen.GameRoom:
                            break;
                        case ActualScreen.Game:
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    Logger.Log($"[S] player {player.NombreUsuario} is on {playerStatus.ActualScreen}.");
                }
            }
        }

        public int GetRegisteredPlayerCount()
        {
            var jugadorDao = new JugadorDAO();
            return jugadorDao.GetTotalJugadores();
        }

        public void SetGameRoomManagerListener(IGameRoomManager gameRoomManager)
        {
            _gameRoomManager = gameRoomManager;
        }

        public void SetGameChatManager(IGameChatManager gameChatManager)
        {
            _gameChatManager = gameChatManager;
        }
    }
}