using System.Linq;
using DataAccess.DAO;
using DataModel.Model;
using LiteNetLib;
using NetworkPacket.Packet.Score;
using Server.Interface;

namespace Server
{
    public class ScoreManager : IScoreManager
    {
        /// <summary>
        ///     Interface de red, para el registro de listeners y envío de paquetes.
        /// </summary>
        private readonly INetworkPacket _networkPacket;

        /// <summary>
        ///     Interface del mánager de salas de juego.
        /// </summary>
        private IGameRoomManager _gameRoomManager;

        /// <summary>
        ///     Interface del manager de player
        /// </summary>
        private readonly IPlayerManager _playerManager;
        
        public ScoreManager(INetworkPacket networkPacket, IPlayerManager playerManager, IGameRoomManager gameRoomManager)
        {
            _networkPacket = networkPacket;
            _playerManager = playerManager;
            _gameRoomManager = gameRoomManager;
            
            _networkPacket.RegisterListener<ScorePacket>(OnScorePacketReceived);
        }

        /// <summary>
        ///     Evento que sucede cuando el servidor recibe un ScorePacket.
        /// </summary>
        /// <param name="scorePacket">Paquete de puntuación</param>
        /// <param name="netPeer">Cliente que envía el paquete</param>
        private void OnScorePacketReceived(ScorePacket scorePacket, NetPeer netPeer)
        {
            var player = _playerManager.FindPlayer(netPeer);
            var sala = _gameRoomManager.GetGameRoomContainingPlayer(player.IdJugador);
            var isPlayerOnGameRoom = sala != null;

            if (isPlayerOnGameRoom)
            {
                var puntajeTotal = new PuntajeTotal(scorePacket);
                var puntajeDao = new PuntajeDAO();
                var score = puntajeDao.AgregarPuntajePorPartida(puntajeTotal);
                
                // TODO Enviar el mismo paquete a los demás jugadores.
                var players = sala.GetPlayers().Where(p => p.IdJugador != player.IdJugador);
                foreach (var p in players)
                {
                    _networkPacket.SendPacket(p.NetPeer, scorePacket, DeliveryMethod.ReliableOrdered);
                }
            }
            else
            {
                var puntajeTotal = new PuntajeTotal(scorePacket);
                var puntajeDao = new PuntajeDAO();
                var score = puntajeDao.AgregarPuntajePorPartida(puntajeTotal);
            }
        }
        
        public void SendScoreToPlayer(Jugador player)
        {
            var puntajeDao = new PuntajeDAO();
            var scores = puntajeDao.GetScoresByUser(player.IdJugador);
                    
            foreach (var score in scores)
            {
                var scorePacket = new ScorePacket
                {
                    Score = score.Puntaje,
                    Kills = score.Asesinatos,
                    Date = score.Fecha.ToString("O")
                };
                _networkPacket.SendPacket(player.NetPeer, scorePacket, DeliveryMethod.ReliableOrdered);
            }
        }

     

        public void SendScoreToPlayer(NetPeer netPeer)
        {
            
        }
    }
}