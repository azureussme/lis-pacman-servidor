using DataAccess.DAO;
using DataAccess.Excepción;
using DataModel.Model;
using DataModel.Model.Enum;
using LiteNetLib;
using NetworkPacket.Enum;
using NetworkPacket.Packet;
using NetworkPacket.Packet.Login;
using Server.Interface;
using Server.Util;

namespace Server
{
    public class LoginManager
    {
        private readonly INetworkPacket _networkPacketInterface;
        private readonly IPlayerManager _playerManager;

        public LoginManager(INetworkPacket networkPacketInterface, IPlayerManager playerManager)
        {
            _networkPacketInterface = networkPacketInterface;
            _networkPacketInterface.RegisterListener<LoginPacket>(OnLoginPacketReceived);
            _networkPacketInterface.RegisterListener<RegisterPacket>(OnRegisterPacketReceived);
            _networkPacketInterface.RegisterListener<VerificationCodePacket>(OnVerificationCodePacketReceived);

            _playerManager = playerManager;
        }

        private void OnLoginPacketReceived(LoginPacket loginPacket, NetPeer peer)
        {
            Logger.Log($"[S] LoginPacket received from {loginPacket.Username}");
            var replyPacket = new ReplyPacket<LoginReplyCode>();
            try
            {
                var jugadorDAO = new JugadorDAO();
                var jugador = jugadorDAO.GetJugadorConCredenciales(loginPacket.Username, loginPacket.Password);
                switch (jugador.EstadoJugador)
                {
                    case EstadoJugador.Registrado:
                        replyPacket.ReplyCode = LoginReplyCode.Success;
                        replyPacket.Message = jugador.IdJugador.ToString();

                        // TODO Checar si el jugador ya tiene una sesión activa.
                        // TODO Should clarify
                        var netJugador = _playerManager.FindPlayer(jugador.IdJugador);
                        if (netJugador != null)
                        {
                            replyPacket.ReplyCode = LoginReplyCode.AlreadyLoggedIn;
                            replyPacket.Message = "Tienes una sesión iniciada en otro lugar.";
                        }
                        else
                        {
                            var netJugador2 = _playerManager.FindPlayer(peer);
                            netJugador2.ActualScreen = ActualScreen.None;
                            netJugador2.IdJugador = jugador.IdJugador;
                            netJugador2.CorreoElectrónico = jugador.CorreoElectrónico;
                            netJugador2.EstadoJugador = jugador.EstadoJugador;
                            netJugador2.NombreUsuario = jugador.NombreUsuario;
                            netJugador2.CreatedAt = jugador.CreatedAt;
                            _playerManager.SendPlayerInfoPacketToPlayer(netJugador2);
                        }
                        break;
                    case EstadoJugador.EnVerificación:
                        replyPacket.ReplyCode = LoginReplyCode.VerificationCodeRequired;
                        replyPacket.Message = jugador.IdJugador.ToString();
                        break;
                    case EstadoJugador.Baneado:
                        replyPacket.ReplyCode = LoginReplyCode.Banned;
                        break;
                    default:
                        replyPacket.ReplyCode = LoginReplyCode.GeneralError;
                        break;
                }
            }
            catch (BusinessLogicException e)
            {
                Logger.Log($"[S] Error at OnLoginPacketReceived: {e.Message}");
                replyPacket.Message = e.Message;
                replyPacket.ReplyCode = e.GetReplyCode<LoginReplyCode>();
            }

            Logger.Log($"[S] Sending LoginReplyPacket for {loginPacket.Username}: {replyPacket.ReplyCode}");
            _networkPacketInterface.SendPacket(peer, replyPacket, DeliveryMethod.ReliableOrdered);
        }

        /// <summary>
        ///     Evento que ocurre cuando se recibe un paquete de registro del cliente. Realiza el registro de un
        ///     jugador, genera un código de verificación y lo notifica al jugador.
        /// </summary>
        /// <param name="registerPacket">Paquete de registro</param>
        /// <param name="peer">Cliente que envió el paquete</param>
        private void OnRegisterPacketReceived(RegisterPacket registerPacket, NetPeer peer)
        {
            Logger.Log($"[S] Solicitud de registro de {registerPacket.Username} con correo {registerPacket.Email}.");

            // Se realiza el registro del jugador y se genera un código de verificación.
            var replyPacket = new ReplyPacket<RegisterReplyCode>();

            try
            {
                var jugador = new Jugador(registerPacket);
                var jugadorDAO = new JugadorDAO();
                var result = jugadorDAO.AgregarJugador(jugador);

                // Enviar correo electrónico
                var verificationCodeGenerator = new VerificationCodeGenerator(jugador);
                verificationCodeGenerator.GenerateCodeForUser();
                // verificationCodeGenerator.SendVerificationCodeToPlayer();

                // Se notifica al jugador que tiene que ingresar su código de verificación.
                replyPacket.ReplyCode = RegisterReplyCode.VerificationCodeRequired;
                replyPacket.Message = jugador.IdJugador.ToString();
                _networkPacketInterface.SendPacket(peer, replyPacket, DeliveryMethod.ReliableOrdered);
            }
            catch (BusinessLogicException e)
            {    
                Logger.Log(e.StackTrace);
                Logger.Log("[S] Error at OnRegisterPacketReceived: " + e.Message);
                replyPacket.ReplyCode = e.GetReplyCode<RegisterReplyCode>();
                replyPacket.Message = e.Message;
                _networkPacketInterface.SendPacket(peer, replyPacket, DeliveryMethod.ReliableOrdered);
            }

            Logger.Log("[S] Sending RegisterReplyPacket: " + replyPacket.ReplyCode);
            _networkPacketInterface.SendPacket(peer, replyPacket, DeliveryMethod.ReliableOrdered);
        }

        /// <summary>
        ///     Evento que ocurre cuando se recibe un paquete de código de verificación del cliente. Realiza la
        ///     verificación de un jugador.
        /// </summary>
        /// <param name="verificationCodePacket">Paquete de código de verificación</param>
        /// <param name="peer">Cliente que envió el paquete</param>
        private void OnVerificationCodePacketReceived(VerificationCodePacket verificationCodePacket, NetPeer peer)
        {
            Logger.Log("[S] Verification code: " + verificationCodePacket.Code + " from " +
                       verificationCodePacket.IdJugador);
            var replyPacket = new ReplyPacket<VerificationReplyCode>();

            var verificationDAO = new VerificaciónDAO();
            try
            {
                verificationDAO.ValidarVerificación(verificationCodePacket.Code, verificationCodePacket.IdJugador);
                replyPacket.ReplyCode = VerificationReplyCode.Success;
            }
            catch (BusinessLogicException e)
            {
                Logger.Log("[S] Error at OnVerificationCodePacketReceived: " + e.Message);
                replyPacket.ReplyCode = e.GetReplyCode<VerificationReplyCode>();
                replyPacket.Message = e.Message;
            }

            Logger.Log("[S] Sending VerificationCodeReplyPacket: " + verificationCodePacket.Code);
            _networkPacketInterface.SendPacket(peer, replyPacket, DeliveryMethod.ReliableOrdered);
        }
    }
}