namespace NetworkPacket.Enum
{
    public enum RegisterReplyCode
    {
        VerificationCodeRequired,
        UsernameAlreadyUsed,
        EmailAlreadyUsed,
        PasswordLowSecurity,
        DatabaseError,
        GeneralError,
        ValidationError
    }
}