namespace NetworkPacket.Enum
{
    public enum LoginReplyCode
    {
        Success,
        
        UsernameDoesNotExist,
        
        PasswordIncorrect,
        
        VerificationCodeRequired,
        
        AlreadyLoggedIn,
        
        Banned,

        DatabaseError,
        
        GeneralError
    }
}