namespace NetworkPacket.Enum
{
    public enum GameEvent
    {
        PowerPelletEaten,
        GhostEaten,
        FruitEaten,
        PacmanDeath,
        LevelClear
    }
}