namespace NetworkPacket.Enum
{
    public enum ActualScreen
    {
        None,
        GameRoomList,
        GameRoom,
        Game,
        PlayerHighscores,
        HighScore,
        GameHighscore,
    }
}