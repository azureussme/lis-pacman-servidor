namespace NetworkPacket.Enum
{
    public enum GameRoomListEvent
    {
        JoinSuccess,
        GameRoomFull,
        GameRoomIncorrectPassword,
        GameRoomDoesntExist
    }
}