namespace NetworkPacket.Enum
{
    public enum VerificationReplyCode
    {
        Success,
        WrongVerificationCode
    }
}