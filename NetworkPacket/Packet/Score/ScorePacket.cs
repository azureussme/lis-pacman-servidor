namespace NetworkPacket.Packet.Score
{
    public class ScorePacket
    {
        public int Score { get; set; }
        
        public int Kills { get; set; }
        
        public int IdJugador { get; set; }
        
        public string Date { get; set; }
        
        public int Position { get; set; }

        public string UserName { get; set; }

    }
}