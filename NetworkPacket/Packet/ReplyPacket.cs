namespace NetworkPacket.Packet
{
    public class ReplyPacket<T> where T : System.Enum
    {
        public T ReplyCode { get; set; }
        public string Message { get; set; }
    }
}