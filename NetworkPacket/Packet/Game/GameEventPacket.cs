using NetworkPacket.Enum;

namespace NetworkPacket.Packet.Game
{
    public class GameEventPacket
    {
        public GameCharacter GameCharacter { get; set; }
        public GameEvent GameEvent { get; set; }
    }
}