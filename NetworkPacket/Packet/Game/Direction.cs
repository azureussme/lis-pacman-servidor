namespace NetworkPacket.Packet.Game
{
    public enum Direction
    {
        Up,
        Left,
        Down,
        Right
    }
}