namespace NetworkPacket.Packet.Game
{
    public enum GameCharacter
    {
        None,
        Pacman,
        Blinky,
        Pinky,
        Inky,
        Clyde,
        Fruit
    }
}