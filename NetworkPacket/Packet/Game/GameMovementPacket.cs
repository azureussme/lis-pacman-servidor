namespace NetworkPacket.Packet.Game
{
    public class GameMovementPacket
    {
        public GameCharacter GameCharacter { get; set; }
        public float X { get; set; }
        public float Y { get; set; }
        public Direction Direction { get; set; }
    }
}