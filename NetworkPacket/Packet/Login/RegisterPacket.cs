namespace NetworkPacket.Packet.Login
{
    public class RegisterPacket
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}