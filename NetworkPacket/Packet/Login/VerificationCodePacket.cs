namespace NetworkPacket.Packet.Login
{
    public class VerificationCodePacket
    {
        public int IdJugador { get; set; }
        public string Code { get; set; }
    }
}