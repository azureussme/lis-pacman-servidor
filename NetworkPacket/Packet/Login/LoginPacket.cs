namespace NetworkPacket.Packet.Login
{
    public class LoginPacket
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}