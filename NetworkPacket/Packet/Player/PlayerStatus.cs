using NetworkPacket.Enum;

namespace NetworkPacket.Packet.Player
{
    public class PlayerStatus
    {
        public int IdJugador { get; set; }
        public ActualScreen ActualScreen { get; set; }
    }
}