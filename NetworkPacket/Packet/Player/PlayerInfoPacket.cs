using System;

namespace NetworkPacket.Packet.Player
{
    public class PlayerInfoPacket
    {
        public int IdPlayer { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string CreatedAt { get; set; }
    }
}