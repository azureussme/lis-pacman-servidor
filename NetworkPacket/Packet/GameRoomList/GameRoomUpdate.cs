namespace NetworkPacket.Packet.GameRoomList
{
    public class GameRoomUpdate
    {
        public string IdGameRoom { get; set; }
        public string Propertary { get; set; }
        public int PlayerCount { get; set; }
        public GameRoomStatus GameRoomStatus { get; set; } 
    }
}