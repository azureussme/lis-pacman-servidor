namespace NetworkPacket.Packet.GameRoomList
{
    public class GameRoomInfoPacket
    {
        public string IdGameRoom { get; set; }
        public string Name { get; set; }
        public string Propertary { get; set; }
        public int PlayerCount { get; set; }
        public int SpectatorsCount { get; set; }
        public bool SpectatorsAllowed { get; set; }
        public bool HasPassword { get; set; }
        public string CreatedAt { get; set; }
    }
}