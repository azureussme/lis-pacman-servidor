namespace NetworkPacket.Packet.GameRoomList
{
    public enum GameRoomStatus
    {
        PreGame,
        InGame,
        Dissolved
    }
}