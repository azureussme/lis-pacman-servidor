namespace NetworkPacket.Packet.GameRoomList
{
    public class CreateGameRoomPacket
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public bool AllowSpectators { get; set; }
    }
}