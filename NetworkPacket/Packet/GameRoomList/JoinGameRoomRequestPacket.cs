using System;

namespace NetworkPacket.Packet.GameRoomList
{
    public class JoinGameRoomRequestPacket
    {
        public string IdGameRoom { get; set; }
        public string Password { get; set; }

        public Guid GetIdGameRoom()
        {
            return new Guid(IdGameRoom);
        }
    }
}