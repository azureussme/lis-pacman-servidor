namespace NetworkPacket.Packet.GameChat
{
    public class GameChatPlayerEvent
    {
        public int IdPlayer { get; set; }
        public string Username { get; set; }
        public string IdGameRoom { get; set; }
        public ChatEvent ChatEvent { get; set; }
    }
}