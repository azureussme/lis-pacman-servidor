namespace NetworkPacket.Packet.GameChat
{
    public class GameChatMessagePacket
    {
        public int IdPlayer { get; set; }
        public string IdGameRoom { get; set; }
        public string Message { get; set; }
    }
}