namespace NetworkPacket.Packet.GameRoom
{
    public enum PlayerEvent
    {
        Join,
        Exit,
        Ready,
        NotReady
    }
}