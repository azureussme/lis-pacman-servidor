namespace NetworkPacket.Packet.GameRoom
{
    public enum GameRoomEvent
    {
        LoadGame,
        GameReady,
        StartGame
    }
}