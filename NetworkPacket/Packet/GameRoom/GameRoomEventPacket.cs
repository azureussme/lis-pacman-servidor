namespace NetworkPacket.Packet.GameRoom
{
    public class GameRoomEventPacket
    {
        public string IdGameRoom { get; set; }
        public GameRoomEvent GameRoomEvent { get; set; }
    }
}