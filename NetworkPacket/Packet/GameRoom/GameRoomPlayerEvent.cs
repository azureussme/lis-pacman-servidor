namespace NetworkPacket.Packet.GameRoom
{
    public class GameRoomPlayerEvent
    {
        public string IdGameRoom { get; set; }
        public int IdJugador { get; set; }
        public string Username { get; set; }
        public PlayerEvent PlayerEvent { get; set; }
    }
}