namespace DataAccess.Excepción
{
    public enum DatabaseExceptionCode
    {
        DuplicateEntry,
        GenericError
    }
}