using System;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Text;
using MySql.Data.MySqlClient;

namespace DataAccess.Excepción
{
    public static class ExceptionHandler
    {
        public static DatabaseException HandleDbUpdateException(DbUpdateException dbUpdateException)
        {
            if (dbUpdateException.InnerException is UpdateException updateException)
            {
                return HandleUpdateException(updateException);
            }
            
            return new DatabaseException("dbUpdateException");
        }

        private static DatabaseException HandleUpdateException(UpdateException updateException)
        {
            if (updateException.InnerException is MySqlException mySqlException)
            {
                return HandleMySqlException(mySqlException);
            }

            return new DatabaseException("updateException");
        }

        private static DatabaseException HandleMySqlException(MySqlException mySqlException)
        {
            return new DatabaseException(mySqlException);
        }

        public static BusinessLogicException HandleDbEntityValidationException(DbEntityValidationException dbEntityValidationException, Enum replyCode)
        {
            string firstError = null;
            var sb = new StringBuilder();
            foreach (var eve in dbEntityValidationException.EntityValidationErrors)
            {
                sb.AppendLine(
                    $"- Entity of type \"{eve.Entry.Entity.GetType().FullName}\" in state \"{eve.Entry.State}\" has the following validation errors:");
                foreach (var ve in eve.ValidationErrors)
                {
                    if (string.IsNullOrEmpty(firstError))
                    {
                        firstError = ve.ErrorMessage;
                    }
                    sb.AppendLine(
                        $"-- Property: \"{ve.PropertyName}\", Value: \"{eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName)}\", Error: \"{ve.ErrorMessage}\"");
                }
            }
            throw new BusinessLogicException(firstError, replyCode);
        }
    }
}