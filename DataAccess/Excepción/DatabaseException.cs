using System;
using MySql.Data.MySqlClient;

namespace DataAccess.Excepción
{
    public class DatabaseException : Exception
    {
        private readonly DatabaseExceptionCode _databaseExceptionCode;

        public DatabaseException(string message) : base(message)
        {
            _databaseExceptionCode = DatabaseExceptionCode.GenericError;
        }
        
        public DatabaseException(MySqlException mySqlException) : base(mySqlException.Message + " [" + mySqlException.Number + "]")
        {
            var number = mySqlException.Number;
            switch (number)
            {
                case 1062:
                    _databaseExceptionCode = DatabaseExceptionCode.DuplicateEntry;
                    break;
                default:
                    _databaseExceptionCode = DatabaseExceptionCode.GenericError;
                    break;
            }
        }

        public DatabaseExceptionCode GetCode()
        {
            return _databaseExceptionCode;
        }
    }
}