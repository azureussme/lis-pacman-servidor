﻿using DataAccess.Util;
using System;
using System.Diagnostics;

namespace DataAccess.Excepción
{
    public sealed class BusinessLogicException : Exception
    {
        private readonly BusinessLogicExceptionCode _businessLogicExceptionCode;
        private readonly Enum _replyCode;
        
        public BusinessLogicException(string message, Enum replyCode) : base(message)
        {
            _replyCode = replyCode;
        }
        
        public BusinessLogicException(string message, BusinessLogicExceptionCode businessLogicExceptionCode) :
            base(message)
        {
            _businessLogicExceptionCode = businessLogicExceptionCode;
            RegistrarExcepciónEnArchivo(message, new StackTrace(1).ToString());
        }

        public BusinessLogicExceptionCode GetCode()
        {
            return _businessLogicExceptionCode;
        }

        public T GetReplyCode<T>() where T : Enum
        {
            return (T) _replyCode;
        }

        private void RegistrarExcepciónEnArchivo(string message, string stacktrace)
        {
            var logger = new Logger();
            var code = GetCode();
            logger.AgregarExcepciónARegistro(message, code, stacktrace);
        }
    }
}
