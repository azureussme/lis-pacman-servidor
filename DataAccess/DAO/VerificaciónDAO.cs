using System.Data.Entity;
using System.Linq;
using DataAccess.Excepción;
using DataAccess.Interface;
using DataModel.Context;
using DataModel.Model;
using DataModel.Model.Enum;
using NetworkPacket.Enum;

namespace DataAccess.DAO
{
    public class VerificaciónDAO : IVerificacionDAO
    {
        public int AgregarVerificación(Verificación verificación)
        {
            using (var db = new DataBaseContext())
            {
                db.Verificacion.Add(verificación);
                return db.SaveChanges();
                // TODO Cachar excepciones
            }
        }

        public void ValidarVerificación(string code, int idJugador)
        {
            using (var db = new DataBaseContext())
            {
                var query = db.Verificacion.FirstOrDefault(v => v.Codigo == code && v.IdJugador == idJugador);
                if (query == null)
                {
                    throw new BusinessLogicException("El código de verificación es inválido.", VerificationReplyCode.WrongVerificationCode);
                }

                var updateJugador = db.Jugador.FirstOrDefault(j => j.IdJugador == idJugador);
                if (updateJugador != null)
                {
                    updateJugador.EstadoJugador = EstadoJugador.Registrado;
                    db.Entry(updateJugador).State = EntityState.Modified;
                    db.Verificacion.Remove(query);
                    db.SaveChanges();
                }
                else
                {
                    // TODO Hubo un problema al actualizar el estado de jugador
                }
                
                // TODO eliminar código de base de datos
            }
        }
    }
}