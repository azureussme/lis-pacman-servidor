using System;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using DataAccess.Excepción;
using DataAccess.Interface;
using DataModel.Context;
using DataModel.Model;
using NetworkPacket.Enum;

namespace DataAccess.DAO
{
    public class JugadorDAO : IJugadorDAO
    {
        public int AgregarJugador(Jugador jugador)
        {
            using (var db = new DataBaseContext())
            {
                db.Jugador.Add(jugador);
                try
                {
                    return db.SaveChanges();
                }
                catch (DbUpdateException dbUpdateException)
                {
                    var databaseException = ExceptionHandler.HandleDbUpdateException(dbUpdateException);
                    switch (databaseException.GetCode())
                    {
                        case DatabaseExceptionCode.DuplicateEntry:
                            // Verificar si existe una verificación en progreso (contraseña incluida)
                            
                            // Check manually for duplicated entries
                            var queryJugadorConCorreoEletrónico = db.Jugador.FirstOrDefault(j =>
                                j.CorreoElectrónico == jugador.CorreoElectrónico);
                            if (queryJugadorConCorreoEletrónico != null)
                            {
                                throw new BusinessLogicException(
                                    "El correo electrónico " + jugador.CorreoElectrónico + " ya está registrado.", RegisterReplyCode.EmailAlreadyUsed);
                            }

                            var queryJugadorConNombreUsuario =
                                db.Jugador.FirstOrDefault(j => j.NombreUsuario == jugador.NombreUsuario);
                            if (queryJugadorConNombreUsuario != null)
                            {
                                throw new BusinessLogicException(
                                    "El nombre de usuario " + jugador.NombreUsuario + " ya está en uso.", RegisterReplyCode.UsernameAlreadyUsed);
                            }

                            break;
                        case DatabaseExceptionCode.GenericError:
                            throw new BusinessLogicException(databaseException.Message, RegisterReplyCode.DatabaseError);
                        default:
                            throw new BusinessLogicException("Error general.", RegisterReplyCode.GeneralError);
                    }
                    throw new BusinessLogicException("Error general.", RegisterReplyCode.GeneralError);
                }
                catch (DbEntityValidationException dbEntityValidationException)
                {
                    throw ExceptionHandler.HandleDbEntityValidationException(dbEntityValidationException,
                        RegisterReplyCode.ValidationError);
                }
            }
        }

        public void EditarJugador(Jugador jugador)
        {
            throw new System.NotImplementedException();
        }

        public void CambiarEstatus(Jugador jugador)
        {
            throw new System.NotImplementedException();
        }

        public Jugador GetJugadorPorId(int idjugador)
        {
            using (var db = new DataBaseContext())
            {
                var jugador = db.Jugador.FirstOrDefault(j => j.IdJugador == idjugador);
                return jugador;
                // TODO Catch exceptions
            }
        }

        public Jugador GetJugadorConCredenciales(string nombreUsuario, string contraseña)
        {
            using (var db = new DataBaseContext())
            {
                var jugador = db.Jugador.FirstOrDefault(j =>
                    j.NombreUsuario == nombreUsuario && j.Contraseña == contraseña);
                if (jugador != null) return jugador;
                
                if (ExisteJugadorConNombreUsuario(db, nombreUsuario))
                {
                    throw new BusinessLogicException("Tu contraseña es incorrecta.", LoginReplyCode.PasswordIncorrect);
                }

                throw new BusinessLogicException("El usuario y contraseña son incorrectos.", LoginReplyCode.UsernameDoesNotExist);
            }
        }

        private bool ExisteJugadorConNombreUsuario(DataBaseContext db, string nombreUsuario)
        {
            var query = db.Jugador.FirstOrDefault(jugador => jugador.NombreUsuario == nombreUsuario);
            return query != null;
        }

        public int GetTotalJugadores()
        {
            using (var db = new DataBaseContext())
            {
                return db.Jugador.Count();
            }
        }
    }
}