using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using DataAccess.Excepción;
using DataAccess.Interface;
using DataModel.Context;
using DataModel.Model;
using NetworkPacket.Enum;

namespace DataAccess.DAO
{
    public class PuntajeDAO : IPuntajeTotalDAO
    {
        public int AgregarPuntajePorPartida(PuntajeTotal puntajeTotal)
        {
            using (var db = new DataBaseContext())
            {
                db.Puntajetotal.Add(puntajeTotal);
                return db.SaveChanges();
                // TODO Cachar excepciones
            }
        }

        public IEnumerable<PuntajeTotal> GetScoresByUser(int idjugador)
        {
            using (var db = new DataBaseContext())
            {
                var query = db.Puntajetotal.Where(p => p.IdJugador == idjugador);
                Console.WriteLine("Tus puntaciones son:" +query);

                return query;
            }

            return null;
        }
    }
}

