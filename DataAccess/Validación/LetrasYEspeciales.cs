﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace DataModel.Validación
{
    class LetrasYEspeciales : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            return Regex.IsMatch((string) value, @"(?=.*[a - z])(?=.*[A - Z])(?=.*\d)(?=.*[^\da - zA - Z]).{ 8,100}$");
        }
    }
}
