﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace DataModel.Validación
{
    class SoloAlfanumericos : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            return Regex.IsMatch((string)value, @"/([A-Z][a-z])\w+/g");
        }
    }
}
