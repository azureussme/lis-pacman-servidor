﻿using DataAccess.Excepción;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Util
{
    public class Logger
    {
        private const string LogFilename = "LISPACMAN.log";

        public void AgregarExcepciónARegistro(string message, BusinessLogicExceptionCode code, string stacktrace)
        {
            using (var streamWriter = new StreamWriter(LogFilename, true))
            {
                streamWriter.Write(DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss") + " " + message + " (" + code +
                                   ")");
                streamWriter.WriteLine();
                streamWriter.Write(stacktrace);
                streamWriter.WriteLine();
                streamWriter.Close();
            }


        }

    }
}
