﻿using DataModel.Model;

namespace DataAccess.Interface
{
    internal interface IVerificacionDAO
    {
        int AgregarVerificación(Verificación verificación);
        void ValidarVerificación(string code, int idJugador);
    }
}
