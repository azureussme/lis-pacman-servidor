﻿using System.Collections.Generic;
using DataModel.Model;

namespace DataAccess.Interface
{
    internal interface IPuntajeTotalDAO
    {
        /// <summary>
        ///     Agrega el puntaje obtenido por usuario a la Base de Datos 
        /// </summary>
        int AgregarPuntajePorPartida(PuntajeTotal puntajeTotal);
        
        IEnumerable<PuntajeTotal> GetScoresByUser(int idjugador);
    }
}
