﻿using DataModel.Model;

namespace DataAccess.Interface
{
    /// <summary>
    ///     Iterfaz para el DAO de jugador
    /// </summary>
    internal interface IJugadorDAO
    {
        /// <summary>
        ///     Agrega un nuevo Jugador a la base de datos
        /// </summary>
        int AgregarJugador(Jugador jugador);

        /// <summary>
        ///     Obtiene un jugador por su IdJugador 
        /// </summary>
        void EditarJugador(Jugador jugador);


        /// <summary>
        ///     Obtiene el estado actual del perfil del jugador
        /// </summary>
        void CambiarEstatus(Jugador jugador);

        Jugador GetJugadorPorId(int idjugador);

        Jugador GetJugadorConCredenciales(string nombreUsuario, string contraseña);

    }
}
